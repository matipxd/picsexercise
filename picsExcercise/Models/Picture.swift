//
//  Picture.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

class Picture: Codable {
    let id: Int
    let author: String
    lazy var photoUrl: URL? = {
        return URL(string: "https://picsum.photos/200?image=\(id)")
    }()
}
