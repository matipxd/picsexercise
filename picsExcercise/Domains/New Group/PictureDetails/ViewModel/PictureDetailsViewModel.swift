//
//  PictureDetailsViewModel.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

class PictureDetailsViewModel {
    private(set) var name: Observable<String> = Observable("")
    private(set) var imageUrl: Observable<URL?> = Observable(nil)

    private var picture: Picture

    init(picture: Picture) {
        self.picture = picture

        self.name.value = picture.author
        self.imageUrl.value = picture.photoUrl
    }
}
