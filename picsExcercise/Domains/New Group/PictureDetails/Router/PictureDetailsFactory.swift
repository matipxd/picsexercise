//
//  PictureDetailsFactory.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

class PicturesDetailsFactory {
    class func create(picture: Picture) -> PictureDetailsViewController {
        let viewController: PictureDetailsViewController = PictureDetailsViewController.instantiateViewController()
        let viewModel = PictureDetailsViewModel(picture: picture)
        viewController.viewModel = viewModel
        return viewController
    }
}
