//
//  PictureDetailsViewController.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import UIKit

extension PictureDetailsViewController {
    enum PictureDetailsSection: Int, CaseIterable {
        case headerImage
        case author
    }
}

class PictureDetailsViewController: UITableViewController, StoryboardInstantiable {
    var viewModel: PictureDetailsViewModel!

    override func viewDidLoad() {
        bind()
        setupNavigationBar()
    }

    private func setupNavigationBar() {
        guard let _ = viewModel.imageUrl.value else {
            return
        }

        let shareBarItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem:.action, target: self, action: #selector(didTapShareButton))

        self.navigationItem.rightBarButtonItem = shareBarItem
    }

    @objc private func didTapShareButton() {
        guard let imageUrl = viewModel.imageUrl.value else {
            return
        }

        let items = [imageUrl]
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(activityViewController, animated: true)
    }

    private func bind() {
        viewModel.name.observe(on: self) { [weak self] in
            self?.tableView.reloadData()
        }

        viewModel.imageUrl.observe(on: self) { [weak self] in
            self?.tableView.reloadData()
        }
    }

    //MARK: - UITableView delegate

    override func numberOfSections(in tableView: UITableView) -> Int {
        return PictureDetailsSection.allCases.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch PictureDetailsSection(rawValue: indexPath.section)! {
        case .headerImage:
            let cell: PictureDetailsImageCell = tableView.dequeueReusableCell(for: indexPath)
            cell.setup(with: viewModel.imageUrl.value)
            return cell
        case .author:
            let cell: PictureDetailsAuthorCell = tableView.dequeueReusableCell(for: indexPath)
            cell.setup(with: viewModel.name.value)
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
}
