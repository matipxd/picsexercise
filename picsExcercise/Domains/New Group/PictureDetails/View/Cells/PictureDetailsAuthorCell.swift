//
//  PictureDetailsAuthorCell.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import UIKit

class PictureDetailsAuthorCell: UITableViewCell {
    @IBOutlet weak var authorLabel: UILabel!

    func setup(with name: String) {
        authorLabel.text = name
    }
}
