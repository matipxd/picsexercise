//
//  PictureDetailsImageCell.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import UIKit

class PictureDetailsImageCell: UITableViewCell {
    @IBOutlet weak var pictureImageView: UIImageView!

    func setup(with image: URL?) {
        guard let image = image else {
            //TODO: add no photo
            pictureImageView.image = nil
            return
        }
        pictureImageView.kf.setImage(with: image)
    }
}
