//
//  PicturesViewModel.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

class PicturesViewModel {
    private(set) var pictures: Observable<[Picture]> = Observable([Picture]())
    private(set) var isLoading: Observable<Bool> = Observable(false)
    private(set) var error: Observable<Error?> = Observable(nil)

    private let picturesService: PicturesServiceProtocol
    var router: PicturesRouterProtocol!

    init(picturesService: PicturesServiceProtocol) {
        self.picturesService = picturesService
    }

    func viewDidLoad() {
        isLoading.value = true

        picturesService.getPictures { [weak self] (result: ServiceResult<[Picture]>) in
            guard let weakSelf = self else {
                return
            }

            defer {
                weakSelf.isLoading.value = false
            }

            guard let pictures = result.value else {
                weakSelf.error.value = result.error
                return
            }

            weakSelf.pictures.value = pictures
        }
    }

    func openPicture(_ picture: Picture) {
        router.openPictureDetails(for: picture)
    }
}
