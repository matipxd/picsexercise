//
//  PictureCell.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class PictureCell: UICollectionViewCell {
    @IBOutlet weak var pictureImageView: UIImageView!
    
    func setup(with picture: Picture) {
        guard let photoUrl = picture.photoUrl else {
            return
        }
        
        pictureImageView.kf.setImage(with: photoUrl)
    }

    override func prepareForReuse() {
        pictureImageView.kf.cancelDownloadTask()
    }
}
