//
//  PicturesViewController.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import UIKit
import CHTCollectionViewWaterfallLayout
import SVProgressHUD

class PicturesViewController: UICollectionViewController, CHTCollectionViewDelegateWaterfallLayout, StoryboardInstantiable {
    var viewModel: PicturesViewModel = PicturesViewModel(picturesService: PicturesService())
    private var collectionLayout = CHTCollectionViewWaterfallLayout()

    override func viewDidLoad() {
        setupCollectionView()
        bind()
    }

    private func bind() {
        viewModel.pictures.observe(on: self) { [weak self] (pictures: [Picture]) in
            self?.collectionView.reloadData()
        }

        viewModel.error.observe(on: self) { [weak self] (error: Error?) in
            self?.showError()
        }

        viewModel.isLoading.observe(on: self) { [weak self] (value: Bool) in
            self?.showLoading(value)
        }

        viewModel.viewDidLoad()
    }

    private func showError() {
        //TODO: make strings file
        SVProgressHUD.showError(withStatus: "Something wrong happend :(")
    }

    private func showLoading(_ isLoading: Bool) {
        if isLoading {
            SVProgressHUD.show(withStatus: "loading")
        } else {
            SVProgressHUD.dismiss()
        }
    }

    private func setupCollectionView() {
        collectionView.alwaysBounceVertical = true
        collectionLayout.minimumColumnSpacing = 6
        collectionLayout.minimumInteritemSpacing = 3
        collectionLayout.columnCount = 2
        collectionLayout.itemRenderDirection = .chtCollectionViewWaterfallLayoutItemRenderDirectionShortestFirst

        collectionView.collectionViewLayout = collectionLayout
    }

    //MARK: - CollectionView delegate

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.pictures.value.count
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PictureCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.setup(with: viewModel.pictures.value[indexPath.row])

        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.openPicture(viewModel.pictures.value[indexPath.row])
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let screenWidth = collectionView.frame.size.width
        let layout = collectionView.collectionViewLayout as! CHTCollectionViewWaterfallLayout
        let columnsCount = CGFloat(layout.columnCount)
        let itemsSpacing = layout.sectionInset.left + layout.sectionInset.right + (columnsCount - 1) * layout.minimumColumnSpacing
        let maxCellWidth = (screenWidth - itemsSpacing) / columnsCount

        //TODO: Make height dynamic, CHTCollectionViewWaterfallLayout supports different cells size
        //TODO: Performance at the beggining is low, need to divide list to smaller and incrementaly load
        return CGSize(width: maxCellWidth, height: maxCellWidth)
    }
}


