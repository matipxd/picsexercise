//
//  PicturesFactory.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

class PicturesFactory {
    class func create() -> PicturesViewController {
        let viewController: PicturesViewController = PicturesViewController.instantiateViewController()
        let viewModel = PicturesViewModel(picturesService: PicturesService())
        let router = PicturesRouter(view: viewController)
        viewController.viewModel = viewModel
        viewModel.router = router
        return viewController
    }
}
