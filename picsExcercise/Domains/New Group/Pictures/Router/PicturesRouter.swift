//
//  PicturesRouter.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import UIKit

protocol PicturesRouterProtocol {
    func openPictureDetails(for picture: Picture)
}

class PicturesRouter: PicturesRouterProtocol {
    private weak var view: UIViewController?

    init(view: UIViewController) {
        self.view = view
    }

    func openPictureDetails(for picture: Picture) {
        let viewController = PicturesDetailsFactory.create(picture: picture)
        view?.navigationController?.pushViewController(viewController, animated: true)
    }
}
