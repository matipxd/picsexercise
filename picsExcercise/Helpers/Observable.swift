//
//  Observable.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation

protocol ObservableProtocol {
    func observe(on observer: AnyObject, observerBlock: @escaping () -> Void)
    func remove(observer: AnyObject)
}

protocol ObserverProtocol {
    func fire(oldValue: Any, newValue: Any)
    var observer: AnyObject? { get }
}

class EmptyObserver: ObserverProtocol {
    typealias ObserverBlock = () -> Void

    weak var observer: AnyObject?
    let block: ObserverBlock

    init(observer: AnyObject, block: @escaping ObserverBlock) {
        self.observer = observer
        self.block = block
    }

    func fire(oldValue: Any, newValue: Any) {
        block()
    }
}

class ValueChangedObserver<Value>: ObserverProtocol {
    typealias ObserverBlock = (_ oldValue: Value, _ newValue: Value) -> Void

    weak var observer: AnyObject?
    let block: ObserverBlock

    init(observer: AnyObject, block: @escaping ObserverBlock) {
        self.observer = observer
        self.block = block
    }

    func fire(oldValue: Any, newValue: Any) {
        block(oldValue as! Value, newValue as! Value)
    }
}

class Observer<Value>: ObserverProtocol {
    typealias ObserverBlock = (_ value: Value) -> Void

    weak var observer: AnyObject?
    let block: ObserverBlock

    init(observer: AnyObject, block: @escaping ObserverBlock) {
        self.observer = observer
        self.block = block
    }

    func fire(oldValue: Any, newValue: Any) {
        block(newValue as! Value)
    }
}

public class ObservableCollection: ObservableProtocol {
    private var observers = [ObserverProtocol]()
    var observer: AnyObject?

    private var observables: [ObservableProtocol]

    init(_ observables: ObservableProtocol...) {
        self.observables = observables
    }

    func observe(on observer: AnyObject, observerBlock: @escaping () -> Void) {
        for observable in observables {
            observable.observe(on: observer, observerBlock: observerBlock)
        }
    }

    func remove(observer: AnyObject) {
        for observable in observables {
            observable.remove(observer: observer)
        }
    }
}

public class Observable<Value>: ObservableProtocol {
    private var observers = [ObserverProtocol]()
    private var _value: Value

    public var value: Value {
        get {
            return _value
        }
        set {
            let oldValue = _value
            _value = newValue
            notifyObservers(oldValue: oldValue, newValue: _value)
        }
    }

    public func update(value: Value, silentlyFor observer: AnyObject) {
        let oldValue = _value
        _value = value
        notifyObservers(oldValue: oldValue, newValue: _value, excludedObserver: observer)
    }

    public init(_ value: Value) {
        _value = value
    }

    func observe(on observer: AnyObject, observerBlock: @escaping EmptyObserver.ObserverBlock) {
        observers.append(EmptyObserver(observer: observer, block: observerBlock))
    }

    func observe(on observer: AnyObject, observerBlock: @escaping Observer<Value>.ObserverBlock) {
        observers.append(Observer(observer: observer, block: observerBlock))
    }

    func observe(on observer: AnyObject, observerBlock: @escaping ValueChangedObserver<Value>.ObserverBlock) {
        observers.append(ValueChangedObserver(observer: observer, block: observerBlock))
    }

    func observeAndFire(on observer: AnyObject, observerBlock: @escaping EmptyObserver.ObserverBlock) {
        observers.append(EmptyObserver(observer: observer, block: observerBlock))
        observerBlock()
    }

    func observeAndFire(on observer: AnyObject, observerBlock: @escaping ValueChangedObserver<Value>.ObserverBlock) {
        observers.append(ValueChangedObserver(observer: observer, block: observerBlock))
        observerBlock(value, value)
    }

    func observeAndFire(on observer: AnyObject, observerBlock: @escaping Observer<Value>.ObserverBlock) {
        observers.append(Observer(observer: observer, block: observerBlock))
        observerBlock(value)
    }

    func has(observer: AnyObject) -> Bool {
        return observers.contains(where: { $0.observer === observer })
    }

    func remove(observer: AnyObject) {
        observers = observers.filter { $0.observer !== observer }
    }

    private func notifyObservers(oldValue: Value, newValue: Value, excludedObserver: AnyObject? = nil) {
        let observers = self.observers.filter { $0.observer !== excludedObserver }

        for observer in observers {
            observer.fire(oldValue: oldValue, newValue: newValue)
        }
    }
}
