//
//  PicturesService.swift
//  picsExcercise
//
//  Created by mpluciak on 11/04/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import Alamofire

protocol PicturesServiceProtocol {
    func getPictures(completed: @escaping (_ result: ServiceResult<[Picture]>) -> Void)
}

enum NetworkServiceError: Error {
    case noData
}

//TODO: Create separate NetworkService
class PicturesService: PicturesServiceProtocol {
    func getPictures(completed: @escaping (_ result: ServiceResult<[Picture]>) -> Void) {
        Alamofire.request("https://picsum.photos/list/")
            .responseData { response in
                if let errorResponse = response.error {
                    completed(ServiceResult.failure(errorResponse))
                    return
                }

                guard let data = response.data else {
                    completed(ServiceResult.failure(NetworkServiceError.noData))
                    return
                }

                let decoder = JSONDecoder()

                do {
                    let decodedResult = try decoder.decode([Picture].self, from: data)
                    completed(ServiceResult.success(decodedResult))
                } catch {
                    completed(ServiceResult.failure(error))
                }
        }
    }
}
