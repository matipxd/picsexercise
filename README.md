# How to test the project

1. To build the project you need to install CocoaPods by using command: `sudo gem install cocoapods`
2. In terminal open project directory and use command: `pod install` to install all necessary pods
3. CocoaPods will create `picsExcercise.xcworkspace` file, use it to open project in xcode
4. Now you can choose simulator and run the exercise :)

# Architecture

- Each module has three layers: View (ViewControllers, cells), ViewModel, Router (responsible for navigating between modules)
- ViewModel notify by using `Observable` View about changes in the property�s value
- ViewModel use router to navigate to other module (here for example to Pic details screen)
- To gather data use Service for example `PicturesService`

What's missing:

- I didn't have enough time to create Dependency container unfortunately 
- Unit tests of ViewModel - I would use Cuckoo to generate mock for `PicturesServiceProtocol` and then would be able to inject into VM

# Services

- Created `PicturesService` which uses Alamofire framework to get data from backend
- To map JSON into classes I used a native Swift solution - `Codable`
- `PictureServices` should use more generic class like `NetworkService` where should be generic method to get data in specific type 

# Pods

To improve development time I decided to use CocoaPods  
I decided to use those pods:

- Alamofire - HTTP networking library, removes a bit boilerplate code in comparition to use URLSession
- Kingfisher - a framework for downloading and caching images from the web
- SVProgressHUD - a library to display loading indicator
- CHTCollectionViewWaterfallLayout - helps create grid like Pinterest, but I didn't read carefully requirements where is note about squares

Note: In the podfile I had to set for some pods lower version of Swift because of compatibility (some libs still are not ready for newest version)

# Screens

- Pictures screen list performance needs to be checked. API doesn't provide option to fetch in chunks images and measuring cells height for all 900 images reduce performance.  Quick workaround for that would be dividing the whole list of images to smaller parts and implement incremental fetching in ViewController and ViewModel

# Sugar code

- Created special extension for UITableView and UICollectionView to `dequeueReusableCell`, don't need to set `ReuseIdentifier` 
- Created special extension for UIViewController to create ViewController using Storyboard without specifying identifier

# Git works

A bit of stress and I am not happy with the way how I worked with GIT. Definitely the changes should be committed one by one not whole project in one commit. I did some smaller fixes in seperate commits ;) 

